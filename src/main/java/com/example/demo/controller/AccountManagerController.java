package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.AccountManager;
import com.example.demo.service.AccountManagerService;

@RestController
@RequestMapping("/account")
public class AccountManagerController {
	@Autowired
	private AccountManagerService accountManagerService;
	
	@PostMapping
	private AccountManager save(@RequestBody AccountManager accountManager) {
		return accountManagerService.save(accountManager);
	}

	@GetMapping
	private List<AccountManager> getAccountManager(){
		return accountManagerService.getAllAccount();
	}
}
