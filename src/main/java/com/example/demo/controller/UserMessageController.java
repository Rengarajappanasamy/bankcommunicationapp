package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.UserMessage;
import com.example.demo.service.UserMessageService;

@RestController
@RequestMapping("/usermessage")
public class UserMessageController {
	@Autowired
	private UserMessageService service;
	
	@PostMapping
	private UserMessage save(@RequestBody UserMessage message) {
		return service.save(message);
	}
	
	@GetMapping
	private List<UserMessage> getAllMessages() {
		return service.getMessages();
	}

}
