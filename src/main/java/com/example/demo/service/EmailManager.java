package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import com.example.demo.dao.UserMessageDAO;
import com.example.demo.model.UserMessage;

@Component
public class EmailManager {
	@Autowired
  private UserMessageDAO userMessageDAO;
	@Autowired
	private JavaMailSender mailSender;
  
	public void sendMail() {
		List<UserMessage> userMessageList = userMessageDAO.findByStatus("PENDING");
		
		if(userMessageList!=null && userMessageList.size()>0) {
			
			userMessageList.stream().forEach(s->doMail(s));
		}
			
	}

	private Object doMail(UserMessage mail) {

		try {
			SimpleMailMessage message = new SimpleMailMessage();
	         message.setFrom("appanasamytest@gmail.com");
	         message.setTo(mail.getMailId());
	         
	        
	        
	         
	         message.setSubject(mail.getSubject());
	         message.setText(mail.getMessage());
	         
	         mailSender.send(message);
	         mail.setStatus("SENT");
	         userMessageDAO.save(mail);
		} catch (Exception e) {
			e.printStackTrace();
			mail.setStatus("FAILED");
			userMessageDAO.save(mail);
		}
		return mail;
		
	

	}

}
