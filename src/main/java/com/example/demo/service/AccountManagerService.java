package com.example.demo.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.AccountManagerDAO;
import com.example.demo.exception.AccountNotValidException;
import com.example.demo.model.AccountManager;

@Service
public class AccountManagerService {
	@Autowired
	private AccountManagerDAO accountManagerDAO;

	public AccountManager save(@Valid AccountManager accountManager) {
		try {
			accountManager = accountManagerDAO.save(accountManager);
		}catch(Exception e) {
			throw new AccountNotValidException("Please provide valid account details");
		}
		return accountManager;
	}

	public List<AccountManager> getAllAccount() {
		List<AccountManager> accountManagerList = (List<AccountManager>) accountManagerDAO.findAll();
		return accountManagerList;
	}

}
