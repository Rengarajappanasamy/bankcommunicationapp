package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmailWorker {
	@Autowired
	private EmailManager manager;
	public void run() throws InterruptedException {
		while(true) {
			System.out.println("inside worker");
			manager.sendMail();
			Thread.sleep(10000);
		}
	}

}
