package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.AccountManagerDAO;
import com.example.demo.dao.UserMessageDAO;
import com.example.demo.model.AccountManager;
import com.example.demo.model.UserMessage;

@Service
public class UserMessageService {
    @Autowired
	private UserMessageDAO userMessageDAO;
    @Autowired
    private AccountManagerDAO accountManager;
	public UserMessage save(UserMessage message) {
		if(message.getAccountNumber()!=null) {
			Optional<AccountManager> accountDetail = accountManager.findById(message.getAccountNumber());
			if(accountDetail.isPresent()) {
			AccountManager account=	accountDetail.get();
				message.setAccountNumber(account.getAccountNumber());
				message.setMailId(account.getEmailId());
				message.setStatus("PENDING");
				return userMessageDAO.save(message);
			}
		}else {
			List<AccountManager> accountManagerList = (List<AccountManager>)accountManager.findAll();
			accountManagerList.stream().forEach(a->save(a,message));
		}
		
		return message;
	}
	private void save(AccountManager account, UserMessage message) {
		UserMessage msg = new UserMessage();
		msg.setSubject(message.getSubject());
		msg.setMessage(message.getMessage());
		msg.setAccountNumber(account.getAccountNumber());
		msg.setMailId(account.getEmailId());
		msg.setStatus("PENDING");
		userMessageDAO.save(msg);
	//	return userMessageDAO.save(message);
		
	}
	public List<UserMessage> getMessages() {
	
		return (List<UserMessage>)userMessageDAO.findAll();
	}

}
