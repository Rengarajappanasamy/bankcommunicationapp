package com.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
public class AccountNotValidException extends RuntimeException{
 public AccountNotValidException(String message) {
	// TODO Auto-generated constructor stub
	 super(message);
}
}
