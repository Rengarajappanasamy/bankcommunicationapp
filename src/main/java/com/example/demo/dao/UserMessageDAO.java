package com.example.demo.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.UserMessage;

public interface UserMessageDAO extends CrudRepository<UserMessage, Integer> {
  public List<UserMessage> findByStatus(String status);
}
