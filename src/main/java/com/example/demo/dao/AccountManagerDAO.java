package com.example.demo.dao;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.AccountManager;

public interface AccountManagerDAO extends CrudRepository<AccountManager,Long>{
	
}
