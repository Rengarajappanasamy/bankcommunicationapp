package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.example.demo.service.EmailWorker;

@SpringBootApplication
public class BankCommunicationSystemApplication implements CommandLineRunner{
    @Autowired
	private EmailWorker emailWorker;
    
   
	public static void main(String[] args) {
		SpringApplication.run(BankCommunicationSystemApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		emailWorker.run();
	}

}
